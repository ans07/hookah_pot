<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-car icon-gradient bg-mean-fruit">
                </i>
            </div>
            <div><?= $page_title ?>
            </div>
        </div>
        <div class="page-title-actions">
            <span>Breadcrumbs</span>
        </div>
    </div>
</div>
<div class="row">

    <div class="col">
        <a class="btn btn-primary" href="<?= base_url('admin/website/home/edit') ?>" role="button">Edit</a>
    </div>
</div>