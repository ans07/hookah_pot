<?php $validation = \Config\Services::validation(); ?>
<?php
if (session()->has('message')) {
?>
    <div class="border-t-4 rounded-b px-4 py-3 mb-4 shadow-md <?= session()->getFlashdata('alert-class') ?>" role="alert">
        <p class="font-bold"><?= session()->getFlashdata('message') ?></p>
    </div>
<?php
}
?>
<form action="" method="post" accept-charset="utf-8">
    <div class="form-group">
        <label for="user_email">Email address</label>
        <input type="email" readonly  class="form-control" id="user_email" name="user_email" value="<?php echo $profile['user_email']; ?>">
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
        <!-- Error -->
        <?php if ($validation->getError('password')) { ?>
            <div class="rounded px-4 py-3 mb-4 shadow-md bg-red-100 text-red-900 mt-2" role="alert">
                <p class="font-bold"><?= $error = $validation->getError('password'); ?></p>
            </div>
        <?php } ?>
    </div>
    <div class="form-group">
        <label for="confpassword">Confirm Password</label>
        <input type="password" class="form-control" id="confpassword" name="confpassword" placeholder="Confirm Password">
        <!-- Error -->
        <?php if ($validation->getError('confpassword')) { ?>
            <div class="rounded px-4 py-3 mb-4 shadow-md bg-red-100 text-red-900 mt-2" role="alert">
                <p class="font-bold"><?= $error = $validation->getError('confpassword'); ?></p>
            </div>
        <?php } ?>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>