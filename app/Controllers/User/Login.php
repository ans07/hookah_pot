<?php

namespace App\Controllers\User;

use App\Controllers\BaseController;
use App\Models\UserModel;

class Login extends BaseController
{
	/**
	 * Instance of the main Request object.
	 *
	 * @var HTTP\IncomingRequest
	 */
	protected $request;

	function __construct()
	{

		$this->session = \Config\Services::session();
		$this->session->start();
	}

	public function index()
	{
		helper(['form']);
		if (session()->get('loggedInHookah')) {
			return redirect()->to(base_url('admin/dashboard'));
		} else {
			echo view('user/login');
		}
	}

	public function auth()
	{
		$model = new UserModel();

		$email = $this->request->getVar('user_email');
		$password = $this->request->getVar('user_password');

		$data = $model->where('user_email', $email)->first();
		if ($data) {
			$pass = $data['password'];
			$verify_pass = password_verify($password, $pass);
			if ($verify_pass) {
				$ses_data = [
					'userId'       => $data['user_id'],
					'userName'     => $data['user_name'],
					'userEmail'    => $data['user_email'],
					'userRole'    => $data['user_role'],
					'loggedInHookah'     => TRUE
				];

				$this->session->set($ses_data);
				return redirect()->to(base_url('admin/dashboard'));
			} else {
				$this->session->setFlashdata('message', 'Wrong Password');
				return redirect()->to(base_url('admin/login'));
			}
		} else {
			$this->session->setFlashdata('message', 'Email not Found');
			return redirect()->to(base_url('admin/login'));
		}
	}

	public function logout()
	{
		$session = session();
		$session->destroy();
		return redirect()->to(base_url('admin/login'));
	}
}
