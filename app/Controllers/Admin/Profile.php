<?php namespace App\Controllers\Admin;
  
use App\Controllers\BaseController;
use App\Models\UserModel;
  
class Profile extends BaseController
{
    /**
     * Instance of the main Request object.
     *
     * @var HTTP\IncomingRequest
     */
    protected $request;

    public function index()
    {
        helper(['form']);
        $data = [];
        $this->global['title'] = 'Dashboard';

        $user_id = 1;

        $usermodel = new UserModel();
        $data['profile'] = $usermodel->where('user_id', $user_id)->first();

        if ($this->request->getMethod() == 'post') {
            $rules = [
                'password' => 'required|min_length[6]|max_length[50]',
                'confpassword'  => 'required|matches[password]'
            ];
            $errors = [
                'password' => [
                    'required' => 'Password cannot be empty',
                    'min_length' => 'Password should be minimum 6 characters',
                    'max_length' => 'Password should be maximum 50 characters',
                ],
                'confpassword' => [
                    'required' => 'Password cannot be empty',
                    'matches' => "Confirm Password doesn't match",
                ]
            ];

            if (!$this->validate($rules, $errors)) {
                $data['validation'] = $this->validator;
            } else {
                $postData = $this->request->getVar();

                $input = [
                    'user_name' => "admin",
                    'user_email' => $postData['user_email'],
                    'password' => password_hash($postData['password'], PASSWORD_DEFAULT),
                    'user_role' => 1,
                    'is_active' => 1,
                ];

                $usermodel->update($user_id, $input);

                session()->setFlashdata('message', 'Profile updated Successfully!');
                session()->setFlashdata('alert-class', 'bg-teal-100 border-teal-500 text-teal-900');
                return redirect()->to(base_url('admin/profile'));
            }
        }

        $this->adminIncludes("admin/profile", $this->global, $data , NULL);
    }
}