<?php namespace App\Controllers\Admin;
  
use App\Controllers\BaseController;
  
class HomePage extends BaseController
{
    public function index()
    {
        $data['page_title'] = 'Home Page';
        $this->global['title'] = 'Home Page';

        $this->adminIncludes("admin/home/view", $this->global, $data , NULL);
    }

    public function update()
    {
        $data['page_title'] = 'Home Page Update';
        $this->global['title'] = 'Home Page Update';

        $this->adminIncludes("admin/home/edit", $this->global, $data , NULL);
    }
}