<?php namespace App\Controllers\Admin;
  
use App\Controllers\BaseController;
  
class Dashboard extends BaseController
{
    public function index()
    {
        $data = [];
        $this->global['title'] = 'Dashboard';

        $this->adminIncludes("admin/dashboard", $this->global, $data , NULL);
    }
}