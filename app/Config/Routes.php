<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
// Website
$routes->get('/', 'Home::index');

// Admin Login
$routes->get('admin/login', 'User\Login::index');
$routes->match(['get','post'], 'admin/login/auth', 'User\Login::auth');
$routes->get('admin/logout', 'User\Login::logout', ['filter' => 'auth']);

// Dashboard
$routes->get('admin/dashboard', 'Admin\Dashboard::index', ['filter' => 'auth']);
$routes->get('admin/profile', 'Admin\Profile::index', ['filter' => 'auth']);

// Home page
$routes->get('admin/website/home', 'Admin\HomePage::index', ['filter' => 'auth']);
$routes->match(['get','post'],'admin/website/home/edit', 'Admin\HomePage::update', ['filter' => 'auth']);

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
